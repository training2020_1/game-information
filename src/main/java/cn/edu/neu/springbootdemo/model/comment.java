package cn.edu.neu.springbootdemo.model;

public class comment {
	private int commentid;
	private int newsid;
	private String comment;
	private String commentime;
	private String sun;
	private int user2id;
	private String username;
	public comment() {
		
	}
	public int getCommentid() {
		return commentid;
	}
	public void setCommentid(int commentid) {
		this.commentid = commentid;
	}
	public int getNewsid() {
		return newsid;
	}
	public void setNewsid(int newsid) {
		this.newsid = newsid;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getCommentime() {
		return commentime;
	}
	public void setCommentime(String commentime) {
		this.commentime = commentime;
	}
	public String getSun() {
		return sun;
	}
	public void setSun(String sun) {
		this.sun = sun;
	}
	public int getUser2id() {
		return user2id;
	}
	public void setUser2id(int user2id) {
		this.user2id = user2id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	@Override
	public String toString() {
		return "comment [commentid=" + commentid + ", newsid=" + newsid + ", comment=" + comment + ", commentime="
				+ commentime + ", sun=" + sun + ", user2id=" + user2id + ", username=" + username + "]";
	}
	
}
