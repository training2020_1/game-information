package cn.edu.neu.springbootdemo.model;

import java.sql.Date;

import org.springframework.web.multipart.MultipartFile;

public class News {
	private int newsid;
	private int userid;
	private String content;
	private String createtime;
	private String firstpicture;
	private String title;
	private MultipartFile file;
	private String username;
	public News() {}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public int getNewsid() {
		return newsid;
	}
	public void setNewsid(int newsid) {
		this.newsid = newsid;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getCreatetime() {
		return createtime;
	}
	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}
	
	public String getFirstpicture() {
		return firstpicture;
	}
	public void setFirstpicture(String firstpicture) {
		this.firstpicture = firstpicture;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return "News [newsid=" + newsid + ", userid=" + userid + ", content=" + content + ", createtime=" + createtime
				+ ", firstpicture=" + firstpicture + ", title=" + title + ", file=" + file + ", username=" + username
				+ "]";
	}
	
	
	
	
}
