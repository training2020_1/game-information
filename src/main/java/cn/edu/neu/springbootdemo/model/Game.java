package cn.edu.neu.springbootdemo.model;

public class Game {
	private int gameid;
	private int userid;
	private String test;
	private String AllGame;
	private String username;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public void setAllGame(String allGame) {
		AllGame = allGame;
	}
	private String createtime;
	@Override
	public String toString() {
		return "Game [gameid=" + gameid + ", userid=" + userid + ", test=" + test + ", AllGame=" + AllGame
				+ ", username=" + username + ", createtime=" + createtime + ", gamepicture=" + gamepicture + ", title="
				+ title + "]";
	}
	private String gamepicture;
	private String title;
	public int getGameid() {
		return gameid;
	}
	public void setGameid(int gameid) {
		this.gameid = gameid;
	}
	public int getUserid() {
		return userid;
	}
	public void setUserid(int userid) {
		this.userid = userid;
	}
	public String getTest() {
		return test;
	}
	public void setTest(String test) {
		this.test = test;
	}
	public String getCreatetime() {
		return createtime;
	}
	public void setCreatetime(String createtime) {
		this.createtime = createtime;
	}
	public String getGamepicture() {
		return gamepicture;
	}
	public void setGamepicture(String gamepicture) {
		this.gamepicture = gamepicture;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAllGame() {
		// TODO Auto-generated method stub
		return null;
	}
}
