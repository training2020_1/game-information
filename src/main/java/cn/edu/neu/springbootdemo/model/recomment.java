package cn.edu.neu.springbootdemo.model;

public class recomment {
	private int recommentid;
	private String recomment;
	private int reuserid;
	private String fatherusername;
	private int commentid;
	private int user2id;
	private String username;
	private String recommentime;
	private int realid;
	private String realname;
	
	
	public String getRealname() {
		return realname;
	}
	public void setRealname(String realname) {
		this.realname = realname;
	}
	public int getRealid() {
		return realid;
	}
	public void setRealid(int realid) {
		this.realid = realid;
	}
	public String getRecommentime() {
		return recommentime;
	}
	public void setRecommentime(String recommentime) {
		this.recommentime = recommentime;
	}
	public int getRecommentid() {
		return recommentid;
	}
	public void setRecommentid(int recommentid) {
		this.recommentid = recommentid;
	}
	public String getRecomment() {
		return recomment;
	}
	public void setRecomment(String recomment) {
		this.recomment = recomment;
	}
	public int getReuserid() {
		return reuserid;
	}
	public void setReuserid(int reuserid) {
		this.reuserid = reuserid;
	}
	
	public String getFatherusername() {
		return fatherusername;
	}
	public void setFatherusername(String fatherusername) {
		this.fatherusername = fatherusername;
	}
	public int getCommentid() {
		return commentid;
	}
	public void setCommentid(int commentid) {
		this.commentid = commentid;
	}
	public int getUser2id() {
		return user2id;
	}
	public void setUser2id(int user2id) {
		this.user2id = user2id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	@Override
	public String toString() {
		return "recomment [recommentid=" + recommentid + ", recomment=" + recomment + ", reuserid=" + reuserid
				+ ", fatherusername=" + fatherusername + ", commentid=" + commentid + ", user2id=" + user2id
				+ ", username=" + username + ", recommentime=" + recommentime + ", realid=" + realid + ", realname="
				+ realname + "]";
	}
	
	
	
	
	
}
