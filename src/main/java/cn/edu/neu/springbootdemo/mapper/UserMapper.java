package cn.edu.neu.springbootdemo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import cn.edu.neu.springbootdemo.core.common.Page;
import cn.edu.neu.springbootdemo.model.User;

@Mapper//使用@Mapper注解，表示这是操作数据库的mapper，SpringBoot自动扫描
public interface UserMapper {

	User existsUser(User user);

	List<User> getUserList(User user);

	void deleteUser(User user);

	int checkName(User user);

	void addUser(User user);

	User getUser(User user);

	void updateUser(User user);

	List<User> getUserListWithPage(Page<User> page);

}
