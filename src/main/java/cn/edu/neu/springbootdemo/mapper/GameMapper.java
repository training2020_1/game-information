package cn.edu.neu.springbootdemo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import cn.edu.neu.springbootdemo.model.Game;
@Mapper
public interface GameMapper {

	int addGame(Game game);

	List<Game> getGameList(Game game);

	void deleteGame(Game game);

	Game getGame(Game game);

	void updateGame(Game game);

	List<Game> getAllGame();

	List<Game> getNewGame();



}
