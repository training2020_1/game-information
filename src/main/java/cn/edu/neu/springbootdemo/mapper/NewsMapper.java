package cn.edu.neu.springbootdemo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import cn.edu.neu.springbootdemo.core.common.Page;
import cn.edu.neu.springbootdemo.model.News;
import cn.edu.neu.springbootdemo.model.comment;
import cn.edu.neu.springbootdemo.model.recomment;

@Mapper
public interface NewsMapper {

	  int addNews(News news);

	Page<News> getNewsListPage(Page<News> page);

	List<News> getNewsList(News news);

	void deleteNews(News news);

	News getNews(News news);

	void reinput(News news);

	List<comment> getcomment(News news);

	void addcomment(comment co);

	void addrecomment(recomment re);

	int change(recomment re);

	List<recomment> getrecomment(News news);

	String getName(recomment r);

	void addrecomment2(recomment re);

	String getrecommentname(recomment r);

	String getrerecommentname(recomment r);

	List<News> getearlyNews();

	List<News> getNewsList2();

	List<News> getAllNews();

}
