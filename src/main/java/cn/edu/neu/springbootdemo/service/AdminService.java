package cn.edu.neu.springbootdemo.service;

import java.util.List;

import cn.edu.neu.springbootdemo.core.common.Page;
import cn.edu.neu.springbootdemo.model.Game;
import cn.edu.neu.springbootdemo.model.News;
import cn.edu.neu.springbootdemo.model.User;

public interface AdminService {

	Page<News> getNewsListPage(News news);

	List<News> getNewsList(News news);

	List<News> getearlyNews();

	List<News> getNewsList2();

	List<News> getAllNews();

	List<Game> getGameList(Game game);

	List<Game> getAllGame();

	List<Game> getNewGame();

	User existsUser(User user);

}
