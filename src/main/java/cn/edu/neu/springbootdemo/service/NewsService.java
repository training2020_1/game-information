package cn.edu.neu.springbootdemo.service;

import java.util.List;

import cn.edu.neu.springbootdemo.model.News;
import cn.edu.neu.springbootdemo.model.comment;
import cn.edu.neu.springbootdemo.model.recomment;

public interface NewsService {

	boolean addNews(News news);

	boolean deleteNews(News news);

	News getNews(News news);

	boolean reinput(News news);
	
	News getAndConvert(News news);

	List<comment> getcomment(News news);

	boolean addcomment(comment co);

	boolean addrecomment(recomment re);

	List<recomment> getrecomment(News news);

	String getName(recomment r);

	boolean addrecomment2(recomment re);

	String getrecommentname(recomment r);

	String getrerecommentname(recomment r);

	
}
