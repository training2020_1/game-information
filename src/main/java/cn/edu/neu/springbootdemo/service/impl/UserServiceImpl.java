package cn.edu.neu.springbootdemo.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.edu.neu.springbootdemo.core.common.Page;
import cn.edu.neu.springbootdemo.mapper.UserMapper;
import cn.edu.neu.springbootdemo.model.User;
import cn.edu.neu.springbootdemo.service.UserService;


@Service //使用@Service注解，SpringBoot自动扫描
public class UserServiceImpl implements UserService{
	@Autowired
	private UserMapper mapper;

	@Override
	public User existsUser(User user) {
		// TODO Auto-generated method stub
		return mapper.existsUser(user);
	}

	@Override
	public List<User> getUserList(User user) {
		// TODO Auto-generated method stub
		
		return mapper.getUserList(user);
	}

	@Override
	public boolean deleteUser(User user) {
		// TODO Auto-generated method stub
		try {
			mapper.deleteUser(user);
			return true;
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		
	}

	@Override
	public int addUser(User user) {
		// TODO Auto-generated method stub
		if(mapper.checkName(user)==0) {
			try {
				mapper.addUser(user);
				return 0;
			}catch(Exception e) {
				e.printStackTrace();
				return 2;
			}
			}else {
				return 1;
			}
			
	}

	@Override
	public User getUser(User user) {
		// TODO Auto-generated method stub
		return mapper.getUser(user);
	}

	@Override
	public boolean updateUser(User user) {
		// TODO Auto-generated method stub
		try{
			mapper.updateUser(user);
			return true;
		}catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		
	}

//	@Override
//	public Page<User> getUserListWithPage(User user) {
//		// TODO Auto-generated method stub
//		Page<User> page=new Page<User>(10);
//		Map<String,Object> params=new HashMap<String, Object>();
//		params.put("username", user.getUsername());
//		params.put("gender", user.getGender());
//		page.setParams(params);
//		List<User> list=mapper.getUserListWithPage(page);
//		page.setList(list);
//		return page;
//	}
}
