package cn.edu.neu.springbootdemo.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.edu.neu.springbootdemo.core.common.Page;
import cn.edu.neu.springbootdemo.mapper.GameMapper;
import cn.edu.neu.springbootdemo.mapper.NewsMapper;
import cn.edu.neu.springbootdemo.mapper.UserMapper;
import cn.edu.neu.springbootdemo.model.Game;
import cn.edu.neu.springbootdemo.model.News;
import cn.edu.neu.springbootdemo.model.User;
import cn.edu.neu.springbootdemo.service.AdminService;
@Service
public class AdminServiceImpl implements AdminService{

	@Autowired
	UserMapper um;
	@Autowired
	NewsMapper nm;
	@Autowired
	GameMapper gm;
	
	
	@Override
	public Page<News> getNewsListPage(News news) {
		// TODO Auto-generated method stub
		Page<News> page = new Page<News>(5);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("title", news.getTitle());
		page.setParams(params);
		 
		return nm.getNewsListPage(page);
	}



	@Override
	public List<News> getNewsList(News news) {
		// TODO Auto-generated method stub
		
		return nm.getNewsList(news);
	}



	@Override
	public List<News> getearlyNews() {
		// TODO Auto-generated method stub
		return nm.getearlyNews();
	}



	@Override
	public List<News> getNewsList2() {
		// TODO Auto-generated method stub
		return nm.getNewsList2();
	}



	@Override
	public List<News> getAllNews() {
		// TODO Auto-generated method stub
		return nm.getAllNews();
	}



	@Override
	public List<Game> getGameList(Game game) {
		// TODO Auto-generated method stub
		return gm.getGameList(game);
	}



	@Override
	public List<Game> getAllGame() {
		// TODO Auto-generated method stub
		return gm.getAllGame();
	}



	@Override
	public List<Game> getNewGame() {
		// TODO Auto-generated method stub
		return gm.getNewGame();
	}



	@Override
	public User existsUser(User user) {
		// TODO Auto-generated method stub
			// TODO Auto-generated method stub
			return um.existsUser(user);
		}

}
