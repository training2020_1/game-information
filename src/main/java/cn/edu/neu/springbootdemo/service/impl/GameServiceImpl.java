package cn.edu.neu.springbootdemo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.edu.neu.springbootdemo.mapper.GameMapper;
import cn.edu.neu.springbootdemo.mapper.NewsMapper;
import cn.edu.neu.springbootdemo.model.Game;
import cn.edu.neu.springbootdemo.model.News;
import cn.edu.neu.springbootdemo.service.GameService;
@Service
public class GameServiceImpl implements GameService {
	@Autowired
	GameMapper gm;

	@Override
	public boolean addGame(Game game) {
		// TODO Auto-generated method stub
		int result = gm.addGame(game);
		return false;
	}

	@Override
	public boolean deleteGame(Game game) {
		// TODO Auto-generated method stub
		try {
			gm.deleteGame(game);
			return true;
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public Game getGame(Game game) {
		// TODO Auto-generated method stub
		return gm.getGame(game);
	}

	@Override
	public boolean updateGame(Game game) {
		System.out.println("*****");
		System.out.println(game);
		System.out.println("*****");
		try {
			gm.updateGame(game);
			return true;
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public Game getAllGame(Game game) {

			Game g = gm.getGame(game);
			
			String AllGame = g.getAllGame();
		
			System.out.println(AllGame);
			
			return g;
		}

	
}
