package cn.edu.neu.springbootdemo.service.impl;

import java.util.List;

import org.apache.ibatis.javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.edu.neu.springbootdemo.core.util.MarkdownUtils;
import cn.edu.neu.springbootdemo.mapper.NewsMapper;
import cn.edu.neu.springbootdemo.model.News;
import cn.edu.neu.springbootdemo.model.comment;
import cn.edu.neu.springbootdemo.model.recomment;
import cn.edu.neu.springbootdemo.service.NewsService;

@Service
public class NewsServiceImpl implements NewsService {
	
	@Autowired
	NewsMapper nm;
	
	
	@Override
	public boolean addNews(News news) {
		// TODO Auto-generated method stub
		int result = nm.addNews(news);
		return false;
	}


	@Override
	public boolean deleteNews(News news) {
		// TODO Auto-generated method stub
		try {
			nm.deleteNews(news);
			return true;
		}
		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
		
	}


	@Override
	public News getNews(News news) {
		// TODO Auto-generated method stub
		
		return nm.getNews(news);
	}


	@Override
	public boolean reinput(News news) {
		// TODO Auto-generated method stub
		System.out.println("*****");
		System.out.println(news);
		System.out.println("*****");
		try {
			nm.reinput(news);
			return true;
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
		
	}


	@Override
	public News getAndConvert(News news) {
		// TODO Auto-generated method stub
		News n = nm.getNews(news);
		
		String content = n.getContent();
	
		System.out.println(content);
		MarkdownUtils mark = new MarkdownUtils();
		System.out.println("lalalala");
//		System.out.println(mark);
		System.out.println("lalalala");
		
		n.setContent(mark.tran(content));
		System.out.println("---------------:"+mark.tran(content));
		return n;
	}


	@Override
	public List<comment> getcomment(News news) {
		// TODO Auto-generated method stub
		
		
		return nm.getcomment(news);
	}


	@Override
	public boolean addcomment(comment co) {
		// TODO Auto-generated method stub
		try {
			nm.addcomment(co);
			return true;
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
		
	}


	@Override
	public boolean addrecomment(recomment re) {
		// TODO Auto-generated method stub
		int cost = nm.change(re);
		try {
			nm.addrecomment(re);
			return true;
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
		
	}


	@Override
	public List<recomment> getrecomment(News news) {
		// TODO Auto-generated method stub
		
		return nm.getrecomment(news);
	}


	@Override
	public String getName(recomment r) {
		// TODO Auto-generated method stub
		
		return nm.getName(r);
	}


	@Override
	public boolean addrecomment2(recomment re) {
		// TODO Auto-generated method stub
		
		try {
			nm.addrecomment2(re);
			return true;
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}
	}


	@Override
	public String getrecommentname(recomment r) {
		// TODO Auto-generated method stub
		
		return nm.getrecommentname(r);
	}


	@Override
	public String getrerecommentname(recomment r) {
		// TODO Auto-generated method stub
		return nm.getrerecommentname(r);
	}


	

}
