package cn.edu.neu.springbootdemo.service;

import cn.edu.neu.springbootdemo.model.Game;

public interface GameService {

	boolean addGame(Game game);

	boolean deleteGame(Game game);


	Game getGame(Game game);

	boolean updateGame(Game game);

	Game getAllGame(Game game);


}
