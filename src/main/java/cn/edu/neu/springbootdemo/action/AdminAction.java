package cn.edu.neu.springbootdemo.action;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.edu.neu.springbootdemo.core.Constants;
import cn.edu.neu.springbootdemo.model.Game;
import cn.edu.neu.springbootdemo.model.News;
import cn.edu.neu.springbootdemo.model.User;
import cn.edu.neu.springbootdemo.service.AdminService;

@Controller
@RequestMapping("/admin")
public class AdminAction {

	@Autowired
	AdminService as;
	
	@RequestMapping("/login") 
	public String login(User user,HttpSession session,Map<String,String> m){
		System.out.println("userlogin:"+user);
		User dbUser=as.existsUser(user);
		//System.out.println(user.getUserName()+","+user.getPassword()+"--------"+user1);
		if(dbUser!=null){		
			session.setAttribute(Constants.LOGIN_USER, dbUser);
			m.put("login", "yes");	
			String redirUrl=(String)session.getAttribute(Constants.ORIGINAL_URL);
			System.out.println("redirUrl:"+redirUrl);
			return "redirect:"+redirUrl;
		}
		else{
			m.put("login",Constants.LOGIN_ERR);	
			return "/index2";
		}
		 
	}
	@RequestMapping("/gotologin")
	public String gotologin()
	{
		return "/home";
	}
	
	@RequestMapping("/gotoAboutMe")
	public String gotoAboutMe(HttpSession session, Model model, News news,Game game)
	{
		User user = (User) session.getAttribute(Constants.LOGIN_USER);
		System.out.println("llllll");
		if(user.getPer().equals("1"))
		{
		session.setAttribute("username", user.getUsername());
		System.out.println(user);

		List<News> list = as.getNewsList(news);
		model.addAttribute("list", list);
		System.out.println(list);
		
		return "/admin/blogs";
		}
		else if(user.getPer().toString().equals("2"))
		{
		session.setAttribute("username", user.getUsername());
		System.out.println(user);
		List<Game> gamelist = as.getGameList(game);
		model.addAttribute("gamelist", gamelist);
		System.out.println(gamelist);
		return "/admin/games";
		}
		else
		{
			return "index2";
		}
	}
	@RequestMapping("/gotoInput")
	public String gotoInput()
	{
		return "/admin/blogs-input";
	}
	
	@RequestMapping("/gotoHome")
	public String gotoHome(Model model)
	{
		List<News> list = as.getearlyNews();
		System.out.println(list);
		model.addAttribute("news", list.get(0));
		System.out.println("((((((((((");
		System.out.println(list.get(0));
		System.out.println("((((((((((");
		List<Game> list2 = as.getNewGame();
		model.addAttribute("game", list2.get(0));
		System.out.println("caonima");
		return "/index2";
		
	}
	
	@RequestMapping("/input")
	public String input()
	{
		return "/input";
	}
	
	@RequestMapping("/gotoblog")
	public String gotoblog()
	{
		return "/admin/blog";
	}
	@RequestMapping("/gotoNews")
	public String gotoNews(Model model)
	{
		List<News> list = as.getAllNews();
		System.out.println(list);
		model.addAttribute("list", list);
		return "/newslist";
	}
	@RequestMapping("Input")
	public String Input()
	{
		return "/admin/games-input";
	}
	@RequestMapping("/gotoGame")
	public String gotoGame(Model model)
	{
		List<Game> list = as.getAllGame();
		System.out.println(list);
		model.addAttribute("list", list);
		return "/game/hotgame";
	}
	
}
