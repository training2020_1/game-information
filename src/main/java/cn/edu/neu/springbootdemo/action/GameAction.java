package cn.edu.neu.springbootdemo.action;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import cn.edu.neu.springbootdemo.core.Constants;
import cn.edu.neu.springbootdemo.core.util.FileUtil;
import cn.edu.neu.springbootdemo.model.Game;
import cn.edu.neu.springbootdemo.model.News;
import cn.edu.neu.springbootdemo.model.User;
import cn.edu.neu.springbootdemo.service.GameService;
import cn.edu.neu.springbootdemo.service.NewsService;

@Controller
@RequestMapping("/game")
public class GameAction {

	@Autowired
	GameService gs;

	@RequestMapping("/gameList")
	public String getGameList() {

		return "/game/gameList";
	}
	@RequestMapping("hotgame")
	public String gethotgame() {
		
		return "/game/hotgame";
	}
	@RequestMapping("/input")
	public String input(Game game, HttpSession session, Model model, HttpServletRequest request)
	{
		System.out.println("上传图片");
		User user = (User) session.getAttribute(Constants.LOGIN_USER);
		System.out.println(user);	
		System.out.println(game);
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateStr = format.format(date);	
		game.setCreatetime(dateStr);       //存时间
		game.setUserid(user.getUserid());  //存用户id
		boolean result = gs.addGame(game);
		model.addAttribute("msg", "发布成功");
		return "forward:/admin/gotoAboutMe";
		
	}
	@RequestMapping("/deleteGame")
	public String deleteGame(Game game, Model model)
	{
		boolean result = gs.deleteGame(game);
		
		if(result) {
			model.addAttribute("msg", "删除成功");
			return "forward:/admin/gotoAboutMe";
		}
		else
		{
			model.addAttribute("msg", "删除失败");
			return "forward:/admin/gotoAboutMe";
		}
	}
		@RequestMapping("/getGame")
		public String getGame(Game game, Model model)
		{
			Game u= gs.getGame(game);
			model.addAttribute("game", u);
			System.out.println("####");
			System.out.println(u);
			System.out.println("####");
			return "/admin/games-reinput";
		}
		@RequestMapping("/updateGame")
		public String updateGame(Game game, Model model)
		{
			System.out.println("$$$$");
			System.out.println(game);
			System.out.println("$$$$");
			boolean result = gs.updateGame(game);
			if(result)
			{
				model.addAttribute("msg", "修改成功");
			}
			else
			{
				model.addAttribute("msg","修改失败");
			}
			return "forward:/admin/gotoAboutMe";
		}
		@RequestMapping("/watchGame")
		public String watchGame(Game game, Model model)
		{
			Game g = gs.getAllGame(game);

			model.addAttribute("game", g);
			
			return "/admin/game";
		}
	}
	