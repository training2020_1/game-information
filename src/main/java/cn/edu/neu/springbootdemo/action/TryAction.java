package cn.edu.neu.springbootdemo.action;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
@Controller
@RequestMapping("/try")
public class TryAction {
	
	@Value("${file-save-path}")
	private String fileSavePath;
	
	@RequestMapping("/upload")
	public String uploadFile(MultipartFile file, HttpServletRequest request, Model model)
	{
		
		
		File dir = new File(fileSavePath);
		if(!dir.exists())
		{
			dir.mkdirs();
		}
		String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
		String newFileName = UUID.randomUUID().toString().replace("-", "")+suffix;
		
		File newFile = new File(fileSavePath+newFileName);
		
		try {
			file.transferTo(newFile);
			//String url = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath()+ "/images/"  + newFileName;
            String url = "/images/"+newFileName;
			model.addAttribute("filename", url);
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		return "/admin/blogs-input";
	}
	
	
	
	
	
	
	
	
	
	
	
//    private String filePath = "D:\\IT\\shixun\\ENV20\\workspace\\springbootdemo\\src\\main\\resources\\static\\images\\";
//
//    // 跳转上传页面
//    @RequestMapping("test")
//    public String test() {
//        return "Page";
//    }
//
//    // 执行上传
//    @RequestMapping("upload")
//    public String upload(@RequestParam("file") MultipartFile file, Model model,HttpServletRequest request) {
//        // 获取上传文件名
//        String filename = file.getOriginalFilename();
//        String fp=request.getServletContext().getRealPath("/springbootdemo/images");
//        System.out.println("-------------------------------------------fp:"+fp);
//        // 定义上传文件保存路径
//        String path = filePath+"rotPhoto/";
//        // 新建文件
//        File filepath = new File(path, filename);
//        // 判断路径是否存在，如果不存在就创建一个
//        if (!filepath.getParentFile().exists()) {
//            filepath.getParentFile().mkdirs();
//        }
//        try {
//            // 写入文件
//            file.transferTo(new File(path + File.separator + filename));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        // 将src路径发送至html页面
//        model.addAttribute("filename", "/images/rotPhoto/"+filename);
//        System.out.println("#####");
//        System.out.println(filename);
//        System.out.println("#####");
//        return "input";
//    }

}
