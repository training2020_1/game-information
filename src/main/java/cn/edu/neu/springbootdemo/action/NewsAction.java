package cn.edu.neu.springbootdemo.action;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import cn.edu.neu.springbootdemo.core.Constants;
import cn.edu.neu.springbootdemo.core.util.FileUtil;
import cn.edu.neu.springbootdemo.model.News;
import cn.edu.neu.springbootdemo.model.User;
import cn.edu.neu.springbootdemo.model.comment;
import cn.edu.neu.springbootdemo.model.recomment;
import cn.edu.neu.springbootdemo.service.NewsService;

@Controller
@RequestMapping("/news")
public class NewsAction {
	
	@Autowired
	NewsService ns;
	
	
	
	@RequestMapping("/input")
	public String input(News news, HttpSession session, Model model, HttpServletRequest request)
	{
		System.out.println("上传图片");
		User user = (User) session.getAttribute(Constants.LOGIN_USER);
		System.out.println("###");
		System.out.println(user);
		System.out.println("###");
		System.out.println(news);
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateStr = format.format(date);	
		news.setCreatetime(dateStr);
		news.setUserid(user.getUserid());
		//上传图片
		
		MultipartFile file=news.getFile();
		if(!file.isEmpty()) {
			String fileName = file.getOriginalFilename();  // 文件名
	        String suffixName = fileName.substring(fileName.lastIndexOf("."));  // 后缀名
	        fileName = UUID.randomUUID().toString().replaceAll("-", "") + suffixName; // 新文件名
	        System.out.println("path:"+request.getServletContext().getRealPath("/images/")); 
	        String filePath="/static/upload/"+fileName;
	       // news.setFirstpicture(filePath);
	        
	        File dest = FileUtil.createFile(filePath);
	        try {
	            file.transferTo(dest);
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
		}

		boolean result = ns.addNews(news);
		model.addAttribute("msg", "发布成功");
		return "forward:/admin/gotoAboutMe";
		
	}
	
	@RequestMapping("/deleteNews")
	public String deleteNews(News news, Model model)
	{
		boolean result = ns.deleteNews(news);
		
		if(result) {
			model.addAttribute("msg", "删除成功");
			return "forward:/admin/gotoAboutMe";
		}
		else
		{
			model.addAttribute("msg", "删除失败");
			return "forward:/admin/gotoAboutMe";
		}
		
		
	}
	
	@RequestMapping("/getNews")
	public String getNews(News news, Model model)
	{
		News u = ns.getNews(news);
		model.addAttribute("news", u);
		System.out.println("####");
		System.out.println(u);
		System.out.println("####");
		return "/admin/blogs-reinput";
	}
	
	@RequestMapping("/reinput")
	public String reinput(News news, Model model)
	{
		System.out.println("$$$$");
		System.out.println(news);
		System.out.println("$$$$");
		boolean result = ns.reinput(news);
		if(result)
		{
			model.addAttribute("msg", "修改成功");
		}
		else
		{
			model.addAttribute("msg","修改失败");
		}
		return "forward:/admin/gotoAboutMe";
	}
	
	@RequestMapping("/watchNews")
	public String watchNews(News news, Model model)
	{
		News u = ns.getAndConvert(news);
		
		
		
		model.addAttribute("news", u);
		
		return "/admin/blog";
	}
	
	@RequestMapping("/watchPing")
	public String watchPing(News news, HttpSession session, Model model)
	{
		System.out.println("新闻名："+ news.getNewsid());
		System.out.println("评论名："+ session.getAttribute(Constants.LOGIN_USER));
		System.out.println("!!!!!!!!!!!!!!");
		List<comment> list = ns.getcomment(news);
		System.out.println(list);
		model.addAttribute("list", list);
		model.addAttribute("newsid", news.getNewsid());
		List<recomment> relist = ns.getrecomment(news);
		System.out.println("看realname");
		System.out.println(relist);
		System.out.println("看realname");
		for(recomment r: relist)
		{
			//String name = ns.getName(r);
			//r.setRealname(name);
//			System.out.println("@@@@@@@@@");
//			System.out.println(r);
//			String name = ns.getName(r);
//			System.out.println("找到的name"+name);
//			r.setRealname(name);
//			System.out.println("@@@@@@@@@");
			if(r.getCommentid()==r.getRealid())
			{
				String fathername =	ns.getrecommentname(r);
				System.out.println("查到的评论fathername"+fathername);
				r.setFatherusername(fathername);
			}
			else
			{
				String fathername = ns.getrerecommentname(r);
				System.out.println("查到的回复评论fathername"+fathername);
				r.setFatherusername(fathername);
			}
			
		}
		System.out.println(relist);
		model.addAttribute("relist", relist);
		return "/admin/ping";
	}
	
	
	@RequestMapping("/addcomment")
	public String addcomment(comment co, HttpSession session)
	{
		
		User user = (User) session.getAttribute(Constants.LOGIN_USER);
		co.setUser2id(user.getUserid());
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateStr = format.format(date);
		co.setCommentime(dateStr);
		System.out.println(co);
		boolean result = ns.addcomment(co);
		System.out.println("进入add页面");
		return "forward:/news/watchPing";
	}
	
	
	@RequestMapping("/reply")
	public String addcomment(comment co, Model model)
	{
		News news = new News();
		news.setNewsid(co.getNewsid());
		List<comment> list = ns.getcomment(news);
		System.out.println(list);
		model.addAttribute("list", list);
		model.addAttribute("newsid", news.getNewsid());
		System.out.println("&&&&&&&&&&&");
		System.out.println(co.getCommentid());
		System.out.println("&&&&&&&&&&&");
		model.addAttribute("commentid", co.getCommentid());
		return "/admin/ping2";
	}
	
	@RequestMapping("/reply2")
	public String addcomment2(recomment re, Model model)
	{
		System.out.println("$$$$$$$$");
		System.out.println(re);
		System.out.println("$$$$$$$$");
		model.addAttribute("commentid",re.getCommentid());
		model.addAttribute("realid", re.getRealid());
		return "/admin/ping3";
	}
	
	
	
	@RequestMapping("/addrecomment")
	public String addrecomment(recomment re, HttpSession session)
	{
		User user = (User) session.getAttribute(Constants.LOGIN_USER);
		re.setReuserid(user.getUserid());
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateStr = format.format(date);
		re.setRecommentime(dateStr);
		re.setRealid(re.getCommentid());
		System.out.println(re);
		
		boolean result = ns.addrecomment(re);
		return "forward:/news/watchPing";
	}
	@RequestMapping("/addrecomment2")
	public String addrecomment2(recomment re, HttpSession session)
	{
		User user = (User) session.getAttribute(Constants.LOGIN_USER);
		re.setReuserid(user.getUserid());
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateStr = format.format(date);
		re.setRecommentime(dateStr);
		System.out.println(re);
		boolean result = ns.addrecomment2(re);
		return "index2";
	}
	
}
